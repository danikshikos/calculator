// module.exports = (num1, num2, zn) => {
//     if (!(isNaN(num1) || isNaN(num2))){
//         num1 = Number(num1);
//         num2 = Number(num2);
//         switch (zn) {
//             case "+":
//                 if (isFinite(num1+num2))
//                 return num1+num2;
//                 else return "Превышение допустимого значения!" 
//             case "-":
//                 if (isFinite(num1-num2))
//                 return num1-num2;
//                 else return "Превышение допустимого значения!" 
//             case "*":
//                 if (isFinite(num1*num2))
//                 return num1*num2;
//                 else return "Превышение допустимого значения!" 
//             case "/":
//                 if (num2 === 0)
//                     return "Нельзя делить на 0!"
//                 if (isFinite(num1/num2))
//                 return num1/num2;
//                 else return "Превышение допустимого значения!" 
//             default:
//                 return "Недопустимый знак!";
//         }
//     }
//     return "Введены недопустимые значения!"
// };

module.exports = (num1, num2, zn) => {
    if (!(isNaN(num1) || isNaN(num2))){
        num1 = Number(num1);
        num2 = Number(num2);
        let rez;
        switch (zn) {
            case "+":
                rez = num1/num2;
                break;
            case "-":
                rez = num1/num2;
                break;
            case "*":
                rez = num1/num2;
                break;
            case "/":
                if (num2 === 0)
                    return "Нельзя делить на 0!"
                rez = num1/num2;
                break;
            default:
                return "Недопустимый знак!";
        }
        if (isFinite(rez))
        return "Превышение допустимого значения"
    }
    return "Введены недопустимые значения!"
};
